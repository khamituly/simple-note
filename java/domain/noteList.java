package domain;

import java.util.ArrayList;
import java.util.List;

// list of all notes

public class noteList {
    public static List<note> noteList ;
    private int size;

    noteList(){
        this.noteList = new ArrayList<note>();
    }

    public int getSize(){
        return noteList.size();
    }

    public List<note> getNoteList() {
        return noteList;
    }

    public void setNoteList(note note) {
        this.noteList.add(note);
    }

    public note get(int i) {
        return noteList.get(i);
    }
}

package domain;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

@WebServlet(name = "blogServlet")
public class blogServlet extends HttpServlet {
    noteList noteList = new noteList();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (check(request, response)) {
            noteList = insertNote(noteList,request);
        } else {
            note note = addNote(request, response);
            noteList.setNoteList(note);

        }

        request.setAttribute("note", noteList);

        request.getRequestDispatcher("/jsp/ViewNotes.jsp").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    private note addNote(HttpServletRequest request, HttpServletResponse response) {
        note note = new note();
        note.setTitle(request.getParameter("title"));
        note.setBody(request.getParameter("body"));
        note.setDate(request.getParameter("date"));
        return note;
    }

    private boolean check(HttpServletRequest request, HttpServletResponse response) {
        if (request.getParameter("id") != null) return true;
        return false;
    }

    private noteList insertNote(noteList noteList,HttpServletRequest request){
            noteList.get(Integer.parseInt(request.getParameter("id"))).setDate(request.getParameter("date"));
            noteList.get(Integer.parseInt(request.getParameter("id"))).setTitle(request.getParameter("title"));
            noteList.get(Integer.parseInt(request.getParameter("id"))).setBody(request.getParameter("body"));
            return noteList;
    }

}

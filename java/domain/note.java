package domain;

import java.util.concurrent.atomic.AtomicInteger;

//main class of notes

public class note {
    private static final AtomicInteger count = new AtomicInteger(0);
    private int id;
    private String title;
    private String body;
    private String date;

    public note() {
        this.id = count.incrementAndGet();
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id){
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String data) {
        this.date = data;
    }
}
